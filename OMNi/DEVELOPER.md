# Developer

## Antes de iniciar entrevista saber

- ¿Viene por recomendación o cómo inicio el proceso?
- Resume/CV/LinkedIn
- ¿A qué puesto aplica?
- ¿Qué necesitamos contratar? Alinear con manager, tech leads, people
- Correo de candidato explicando por qué quiere trabajar en nuestra compañía y cuáles son sus expectativas

## Pasos

- Introducción y presentación de las personas.
- Se le da contexto de la empresa.
- Nuestros modelos de negocio, verticales y lo que queremos lograr.
- Explicar lo que esperamos del rol al que aplica.
- Exposición del candidato.
- Realizar preguntas generales para evaluar perfil profesional y fit cultural.
- Realizar preguntas generales de programación.
- Realizar preguntas específicas del stack de tecnologías, herramientas que utilizará con frecuencia.
- Permitir que el candidato resuelva dudas con nosotros de cualquier índole.

## Posibles Preguntas / Banco de preguntas

### Generales

- ¿Está trabajando?
- ¿Cuáles son sus responsabilidades actuales? ¿Cómo se conforma su equipo de trabajo?
- ¿Cuáles son los pasos que lleva a cabo para el desarrollo de tareas?
- ¿Cuáles son las fortalezas por las que se considera un candidato apropiado para el puesto?
- ¿Con qué tecnologías se siente más cómodo y por qué?
- ¿Cuáles han sido sus mayores retos a lo largo de su experiencia laboral?
- ¿Qué busca de una empresa como nosotros?
- ¿Dónde se ve en unos años, haciendo qué?
- ¿Se ve como una persona que le guste enseñar y hacer crecer a otros?

### Para un Trainee / Junior

- ¿Materia/s favorita/s de la universidad? ¿Por qué?
- ¿Cursos favoritos?
- ¿Trabajo ideal?
- ¿Qué cosas lo motivan?
- Se requiere que aquellas preguntas técnicas y de gestión de tareas, manejo de equipos, si el candidato las desconoce, indique que no las sabe.

### De programación generales

- ¿Qué hace el segundo parámetro de useEffect? **React**
- ¿Cuándo utilizaría context y Redux? Por que? **React**
- ¿Qué es Generics?
- ¿Qué significa encapsulamiento en poo? ¿Y polimorfismo?
- ¿A qué se denomina funciones lambda?
- ¿Qué es una recursión y cuándo se utiliza?
- ¿Cuáles son las diferencias entre stack y heap?
- ¿Qué es GC?
- ¿Cuáles son las diferencias entre paralelismo y concurrencia?
- ¿Qué son los Threads?

### Básico JS

- ¿Qué es pure function?
- ¿Qué es el event loop?
- ¿Qué representa ESNext, ES6?
- ¿Cuándo se usa var, let, const?
- ¿Cuántos hilos se ejecutan en un script JS?
- ¿Qué es closure?
- ¿Se pueden usar clases?
- ¿Qué es un prototipo?
- ¿Cómo se aplica un prototipo?
- ¿Qué es event bubbling? (JS Web)

### Servidores + Cloud

- Mencionar servicios de AWS, Azure, GCP, Digital Ocean, etc.
- ¿Nginx/Apache?
- ¿Qué es AWS Lambda?
- ¿Qué es EC2?
- ¿Qué es Docker? ¿Cuáles son las diferencias entre dockerfile y compose?
- ¿Qué es Kubernetes, ECS?
- Metodos/Verbos HTTP Rest

### Testing

- Menciona herramientas de unit testing.
- ¿En qué se diferencian de las de integración? ¿Y estas últimas con las n2n?

### Project management

- Uso de trello, Jira para el manejó de tickets.
- ¿Scrum? ¿Kanban? Ventajas y desventajas.

### BE patterns

- Repository.
- Dependency injection.
- Layers (MVC, MVVM, Clean Architecture).
- Decorator

### FE

- ¿Quién es el encargado de renderizar una página web?
- ¿Qué es el DOM?
- ¿Qué es AJAX?
- ¿HTML que es un atributo de html?
- ¿Por qué las imágenes deben llevar 'alt'? ¿Qué significa? ¿Para qué sirven los atributos 'data'?
- ¿Qué es un meta tag?
- ¿Qué es y cuáles son las ventajas del Shadow DOM? **Avanzado**
- ¿Dónde es recomendado poner los tag <script/>? ¿Por qué?
- ¿Qué son los selectores de ID en CSS?
- ¿En qué se diferencia el diseño responsivo del diseño adaptable?
- ¿Qué tipos de position existen? ¿cuándo usar cada uno? (absolute, fixed, relative)
- ¿Conocés flexbox y grid? ¿Cuándo usarías cada uno?
- ¿En qué se diferencia el padding del margin?
- ¿Qué Que es un media query?
- ¿Que es un preprocesador? ¿Por qué deberíamos usarlos?
