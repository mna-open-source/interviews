# Tech architect

## Conceptos

- Coreografía y orquestación,
- Atomicidad y consistencia eventual,
- Elasticidad y escalabilidad,
- Commit de dos fases,
- Redundancia de servidores,
- Sharding,
- Clean architecture,
- Monolitos, microlitos, microservicios, serverless
- Arquitectura Event-Driven,
- CQRS,
- SOLID,
- Arquitectura hexagonal,
- Tolerancia a fallos,
- Idempotencia,
- Política de reintentos,
- Exponential backoff

### Algunas referencias

- [Microservicios vs Microlitos vs Monolitos: ventajas y desventajas.](https://www.paradigmadigital.com/techbiz/microservicios-vs-microlitos-vs-monolitos-ventajas-desventajas/)
