package com.mercadolibre.customlist;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CustomList {

    private final static Log logger = LogFactory.getLog(CustomList.class);

    private int[] core;
    private int[] min;
    private int[] max;
    private int size = 0;
    private Integer removed = null;

    public CustomList() {
        this.core = new int[0];
        this.min = new int[0];
        this.max = new int[0];
        logger.debug("Instantiated list");
    }

    public CustomList push(final int element) {
        logger.debug(String.format("Push method called with value %s", element));

        resize(true);

        int last = this.size - 1;
        int previous = last - 1;

        this.core[last] = element;
        this.min[last] = this.size == 1 ? element : (element < this.min[previous] ? element : this.min[previous]);
        this.max[last] = this.size == 1 ? element : (element > this.max[previous] ? element : this.max[previous]);

        logger.debug(this.toString());
        return this;
    }

    public Integer pop() {
        logger.debug("Pop method called");

        this.removed = resize(false);

        logger.debug(this.toString());

        return this.removed;
    }

    public Integer max() {
        return this.size == 0 ? null : this.max[this.size - 1];
    }

    public Integer min() {
        return this.size == 0 ? null : this.min[this.size - 1];
    }

    public Integer lastAdded() {
        return this.size == 0 ? null : this.core[this.size - 1];
    }

    public Integer lastRemoved() {
        return this.removed;
    }

    private Integer resize(final boolean increment) {
        if (!increment && this.size == 0) {
            return null;
        }

        final int length = this.size + (increment ? +1 : -1);

        logger.debug(String.format("Resize method called. Old size: %s; new size: %s", this.size, length));

        final int[] copyCore = new int[length];
        final int[] copyMin = new int[length];
        final int[] copyMax = new int[length];

        final Integer element = increment ? null : this.core[this.size - 1];

        final int copyLength = this.size < length ? this.size : length;

        System.arraycopy(this.core, 0, copyCore, 0, copyLength);
        System.arraycopy(this.min, 0, copyMin, 0, copyLength);
        System.arraycopy(this.max, 0, copyMax, 0, copyLength);

        this.core = copyCore;
        this.min = copyMin;
        this.max = copyMax;
        this.size = length;

        return element;
    }

    @Override
    public String toString() {
        return String.format("Min: %s, Max: %s, Size: %s, Last added: %s, Last removed: %s", this.min(), this.max(), this.size, this.lastAdded(), this.lastRemoved());
    }
}
