package com.mercadolibre.customlist;

public class Main {

    public static void main(String[] args) {
        System.out.println("Lista con métodos de primer orden\n");

        final CustomList list = new CustomList();

        list.push(1);
        list.push(4);
        list.push(0);
        list.push(2);

        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
    }

}
